# TUDO Ticket Downloader

## What is this?

A SvelteKit WebApp for downloading your Semesterticket / Deutschlandticket in PNG-Format as a student at [TU Dortmund University](https://www.tu-dortmund.de)

![](./screenshot.png)

## Developing

Run this on a Linux machine with git and NodeJS 20+ installed:

```bash
git clone https://codeberg.org/jfowl/tudo-ticket-downloader.git
cd tudo-ticket-downloader
npm install
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.
