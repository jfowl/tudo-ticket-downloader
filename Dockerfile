FROM node:21.1-alpine AS node-base

FROM node-base AS build

WORKDIR /app
COPY package.json package-lock.json ./

RUN npm install --ignore-scripts

COPY . .
RUN npm run build

# Add SIGINT handler, so you can ctrl+c kill the docker container:
RUN echo "process.on('SIGINT', function() {process.exit();});" >> /app/build/index.js



FROM node-base AS runtime

EXPOSE 3000

ENTRYPOINT [ "node", "/app/build" ]

# package.json needed for node to use the module format:
COPY package.json /app/
COPY --from=build /app/build /app/build
