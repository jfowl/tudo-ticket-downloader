import type { PageServerLoad, Actions } from './$types';
import { fail } from '@sveltejs/kit';

import TuDoApiClient from '$lib/tudo-api';

export const actions = {
	login: async ({ cookies, request }) => {
		const data = await request.formData();
		let username = data.get('username');
		let password = data.get('password');

		if (!username || !password) {
			console.log('login failed: username and password must be set');
			return fail(400, { message: 'username and password must be set' });
		}
		if (typeof username !== 'string' || typeof password !== 'string') {
			console.log('login failed: username and password must be strings');
			return fail(400, { message: 'username and password must be strings' });
		}

		username = username.trim().toLocaleLowerCase();
		password = password.trim();

		if (username.length < 2 || password.length < 2) {
			console.log('login failed: username and password must be at least 2 characters long');
			return fail(400, { message: 'username and password must be at least 2 characters long' });
		}

		const client = new TuDoApiClient();
		const signedIn = await client.signIn(username, password);
		if (!signedIn) {
			console.log('login failed: invalid username and/or password');
			return fail(401, { message: 'Invalid username and/or password' });
		}

		console.log('Successfully signed in');

		cookies.set('tudo_access_token', client.getAccessToken(), {
			path: '/'
		});

		const ticketIds = await client.listTickets();
		console.log(`Fetched ${ticketIds.length} ticket IDs for user`);

		return { ticketIds: ticketIds };
	}
} satisfies Actions;
