import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import TuDoApiClient from '$lib/tudo-api';

export const GET: RequestHandler = async ({ params, cookies }) => {
	const accessToken = cookies.get('tudo_access_token');
	if (typeof accessToken !== 'string') {
		error(401, { message: 'Not authenticated, missing tudo_access_token' });
	}
	const ticketId = params.ticketId;

	const client = new TuDoApiClient();
	client.setAccessToken(accessToken);
	try {
		const ticketData = await client.getTicketPng(ticketId);
		return new Response(ticketData);
	} catch (err) {
		error(500, { message: 'Failed to fetch ticket data' });
	}
};
