export interface TuDoApiAccessTokenResponse {
	scope: string;
	expires_in: number;
	token_type: 'Bearer';
	refresh_token: string;
	access_token: string;
}

async function decodeBase64(data: string): Promise<Uint8Array> {
	const dataUrl = 'data:application/octet-stream;base64,' + data;
	const res = await fetch(dataUrl);
	return new Uint8Array(await res.arrayBuffer());
}

export default class TuDoApiClient {
	private readonly baseUrl: string;
	private auth: {
		refreshAfter: Date;
		refreshToken: string;
		accessToken: string;
		scopes: string[];
	} | null = null;

	constructor(baseUrl?: string) {
		this.baseUrl = baseUrl ?? 'https://mobil.itmc.tu-dortmund.de/';
	}

	async signIn(username: string, password: string): Promise<boolean> {
		const url = `${this.baseUrl}oauth2/v2/access_token`;
		const authData = {
			username: username,
			password: password,
			grant_type: 'password'
		};

		const response = await fetch(url, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			mode: 'cors',
			body: JSON.stringify(authData)
		});
		if (!response.ok) {
			return false;
		}

		const authResult = await (response.json() as Promise<TuDoApiAccessTokenResponse>);

		if (!authResult?.scope?.includes('uid')) {
			return false;
		}

		const expiresAfter = new Date(Date.now() + authResult.expires_in * 1000);

		this.auth = {
			refreshAfter: expiresAfter,
			refreshToken: authResult.refresh_token,
			accessToken: authResult.access_token,
			scopes: authResult.scope.split(' ')
		};

		return true;
	}

	getAccessToken(): string {
		if (this.auth == null) {
			throw new Error('Not authenticated');
		}
		return this.auth.accessToken;
	}

	setAccessToken(accessToken: string) {
		this.auth = {
			accessToken: accessToken,
			refreshAfter: new Date(),
			refreshToken: '',
			scopes: []
		};
	}

	async listTickets(): Promise<string[]> {
		if (this.auth == null) {
			throw new Error('Not authenticated');
		}

		const url = `${this.baseUrl}ticket/v4/tickets`;
		const response = await fetch(url, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				Authorization: `Bearer ${this.auth.accessToken}`
			},
			mode: 'cors'
		});
		if (!response.ok) {
			throw new Error('Failed to fetch tickets ' + response.statusText);
		}

		const tickets = await response.json();
		return tickets;
	}

	async getTicketPng(ticketId: string): Promise<Uint8Array> {
		if (this.auth == null) {
			throw new Error('Not authenticated');
		}

		const url = `${this.baseUrl}/ticket/v4/tickets/${ticketId}`;
		const response = await fetch(url, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				Authorization: `Bearer ${this.auth.accessToken}`
			},
			mode: 'cors'
		});
		if (!response.ok) {
			throw new Error('Failed to fetch ticket ' + response.statusText);
		}

		const { semesterId, ticketData } = (await response.json()) as {
			semesterId: string;
			ticketData: string;
		};
		if (semesterId != ticketId) {
			throw new Error(
				'returned ticket semester ID (' +
					semesterId +
					') does not match requested ticket ID (' +
					ticketId +
					')'
			);
		}

		// ticketdata is base64 encoded. Use latest web technology to decode that base64 into a byte array:
		const ticketPngBytes = await decodeBase64(ticketData);
		return ticketPngBytes;
	}

	getScopes(): string[] {
		if (this.auth == null) {
			throw new Error('Not authenticated');
		}
		return this.auth?.scopes ?? [];
	}
}
